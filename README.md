# Space X Api Client Demo

Alpha version project that demonstrates the use of Java, JSP, Spring Boot, MVC, REST, Junit 5 Jupiter, Bootstrap, HTML, CSS and JavaDoc

Please feel free to comment on what can be better and realize this is a simple demo. Unlimited features and data processing can be added.

The request was a bit odd but I took it on as a good skills demonstration for my Java Full Stack resume.  It was odd in that ususally the Java backend itself would be the REST server but in this request it acts more as a middle man passing on data.

> "Code Exercise
> Develop a Java Webapp to display information about past and future SpaceX launches and SpaceX
> rockets.
> Requirements
> General
> ● Develop a Java Webapp
> ● Utilize a Java framework such as Spring or similar
> ○
> Consume the SpaceX RESTful API within the Java webapp and return results to be
> displayed in the browser
> ●
> Style and display information in an effective manner. Use the CSS framework of your choice,
> such as Bootstrap.
> SpaceX Rockets
> ●
> ●
> Using the /rockets API endpoint:
> ○ Retrieve and display rocket information
> ○ Include sort functionality within the webapp
> Using the /launches API endpoint:
> ○
> Display a list of launches that the rocket has been and will be involved with
> Criterion
> Some of the things we will be looking for (in no particular order):
> 1.
> Basic requirements have been met
> 2. Code is properly documented
> 3. Code adheres to standard security guidelines
> 4. Code adheres to standard best practices
> 5. Proper syntax
> 6. Proper usage of Java frameworks
> 7. No major bugs found in end-product
> Java Web Developer - Interview Code Exercise2
> Resources
> SpaceX API​ : ​ https://github.com/r-spacex/SpaceX-API/wiki"

Enhancements I would like to add:
* [ ]  Using SpringBoots security to have a log in page
* [ ]  Read in data 1 time and peresist to an in memeory database like H2 and use JPA
* [ ]   Explore various ways of doing integration and unit testing using Spring Boot's tooling
* [ ]  Some data processing examples
* [ ]  Connect with some REACT views pages

Screenshots:
![Semantic description of image](/screenshots/spacexpg1.png "Home Page")
![Semantic description of image](/screenshots/spacexpg2.png "Rockets View")
![Semantic description of image](/screenshots/spacexpg3.png "Launches View")
