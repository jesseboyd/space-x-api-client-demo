package com.jesse.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.jesse.service.RocketList;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
class RocketListTest {

    
    @Autowired
    private RocketList rl; // Rocketlist will not work with autowired fields unless it also is autowired
	
    @Test
    void simpleTest() {
    	assertNotNull(rl);
    	assertEquals(4, rl.getRocketList().size());
    	rl.getRocketList().forEach(System.out::println);
    }

}
