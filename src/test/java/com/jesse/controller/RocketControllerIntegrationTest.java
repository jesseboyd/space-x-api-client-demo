package com.jesse.controller;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jesse.model.Rocket;
import com.jesse.service.RocketList;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(Lifecycle.PER_CLASS)
class RocketControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private RocketList rl; 
    
    @BeforeAll
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
            .build();   
    }

    @Test
    public void callRocketControllerRockets() throws Exception {
        MvcResult result = mockMvc.perform(get("/rockets"))
          .andExpect(status().isOk())
          .andExpect(model().attributeExists("rockets"))
          .andDo(print())
          .andReturn();
 
          @SuppressWarnings("unchecked")
		List<Rocket> rockets =  (List<Rocket>) result.getModelAndView().getModel().get("rockets");
//          rockets.forEach(System.out::println);
          
          assertIterableEquals(rockets, rl.getRocketList());
    }

}
