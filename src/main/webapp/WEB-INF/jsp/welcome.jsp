<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Bootstrap CSS -->
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>Welcome to Jesse's SpaceX Client</title>
</head>
<body class="bg-dark text-white">
	<div class="container bg-dark text-light">
		<h1>SpaceX API Client</h1>
		<img class="mx-auto" src="spaceXrockets.jpg" />
		<nav class="navbar navbar-expand-md navbar-dark bg-dark text-light">
			<a class="navbar-brand"
				href="https://www.linkedin.com/in/mrjesseboyd/">Design by
				Jesse Boyd</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto ">

					<li class="nav-item  border border-primary p-2 m-2"><a
						class="nav-link" href="rockets">Rockets</a></li>
					<li class="nav-item  border border-primary p-2 m-2"><a
						class="nav-link" href="launches">Launches</a></li>
				</ul>

			</div>
		</nav>
	</div>
</body>
</html>