<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<title>Display Rocket Information</title>


<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="/css/boostrap.min.css">

</head>
<body style="background: black; color: white">

	<h1>List of available Rockets</h1>

	<table class="table table-borderd table-dark">
		<thead>
			<tr>
				<th>Name</th>
				<th>Type</th>
				<th>Description</th>
				<th>Payloads</th>
			</tr>
		</thead>
		<c:choose>
			<c:when test="${rockets.size()>0 }">
				<c:forEach items="${rockets}" var="rocket">
					<tr>
						<td><c:out value="${rocket.rocket_name}" /></td>
						<td><c:out value="${rocket.rocket_type}" /></td>
						<td><c:out value="${rocket.description}" /></td>
						<td><c:out value="${rocket.second_stage.payloads}" /></td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<td class="alert alert-danger">Sorry no rockets found matching
					that name</td>
			</c:otherwise>
		</c:choose>
	</table>

	<div class="container">
		<nav class="navbar navbar-expand-md navbar-dark bg-dark text-light">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto ">
					<li class="nav-item  border border-primary p-2 m-2"><a
						class="nav-link" href="/">Home</a></li>
					<li class="nav-item  border border-primary p-2 m-2"><a
						class="nav-link" href="rockets">Show All Rockets</a></li>
					<li class="nav-item  border border-primary p-2 m-2"><a
						class="nav-link" href="sortRockets">Sort Rockets</a></li>
				</ul>
				<form class="form-inline my-2 my-lg-0" method="POST"
					action="rocketName">
					<label>Choose by Name</label> <input class="form-control mr-sm-2"
						type="search" name="name" value="hint: Falcon 1" size="25" />
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit"
						value="Submit" class="btn btn-outline-primary">Search</button>
				</form>
			</div>
		</nav>
	</div>


</body>
</html>