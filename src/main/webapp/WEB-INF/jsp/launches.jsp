<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<style>
nav .navbar-nav li a{
  color: white !important;
  }
</style>
<title>SpaceX Launches</title>
</head>
<body style="background: black; color: white">
	<div class="container ">
		<h1>SpaceX Launches</h1>
		<nav class="navbar navbar-expand-md navbar-dark">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto " >
					<li class="nav-item  border border-primary p-2 m-2"><a
						class="nav-link" href="/">Home</a></li>
					<li class="nav-item  border border-primary p-2 m-2"><a
						class="nav-link" href="allLaunches">Show All Launches</a></li>
					<li class="nav-item  border border-primary p-2 m-2"><a
						class="nav-link" href="pastLaunches">Past Launches</a></li>
					<li class="nav-item  border border-primary p-2 m-2"><a
						class="nav-link" href="futureLaunches">Future Launches</a></li>
				</ul>
			</div>
		</nav>
	</div>
<div class="container border border-info">
<!-- more can be done here with the data but for this exercise, this is sufficient -->
<h2>Launch Data:</h2>
<h3>${dataHeader}</h3>
<blockquote><code>${launches}</code></blockquote>

</div>
</body>
</html>