package com.jesse.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Jesse Boyd
 * simplified model to represent SpaceX rocket, not all values are used from the JSON payload
 * more can be added or removed in future
 *
 */
public class Rocket {

	private String rocket_name;
	private String rocket_type;
	private String description;
	@JsonProperty("second_stage")
	private Second_Stage second_stage;

	public String getRocket_name() {
		return rocket_name;
	}
	
	public Rocket() {
		
	}
	
	public Rocket(String rocket_name, String rocket_type, String description, Second_Stage second_stage) {
		this.rocket_name = rocket_name;
		this.rocket_type = rocket_type;
		this.description = description;
		this.second_stage = second_stage;
	}



	public void setRocket_name(String rocket_name) {
		this.rocket_name = rocket_name;
	}

	public String getRocket_type() {
		return rocket_type;
	}

	public void setRocket_type(String rocket_type) {
		this.rocket_type = rocket_type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Second_Stage getSecond_stage() {
		return second_stage;
	}

	public void setSecond_stage(Second_Stage second_stage) {
		this.second_stage = second_stage;
	}

	@Override
	public String toString() {
		return "Rocket [rocket_name=" + rocket_name + ", rocket_type=" + rocket_type + ", description=" + description
				+ ", second_stage=" + second_stage + "]";
	}

}
