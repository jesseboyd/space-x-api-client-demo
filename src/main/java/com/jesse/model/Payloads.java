package com.jesse.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Jesse Boyd example of 3rd level deep JSON mapping for rockets
 */
@JsonTypeName("payloads")
public class Payloads {

	@JsonProperty("option_1")
	String option1;
	@JsonProperty("option_2")
	String option2;

	public Payloads() {
	}

	public Payloads(String option1, String option2) {
		this.option1 = option1;
		this.option2 = option2;
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	@Override
	public String toString() {
		return "Payloads [option1=" + option1 + ", option2=" + option2 + "]";
	}
}
