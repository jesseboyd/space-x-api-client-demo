package com.jesse.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * @author Jesse Boyd model representation of second_stage which is nested
 *         within rocket json
 *
 */
@JsonTypeName("second_stage")
public class Second_Stage {

	@JsonProperty("payloads")
	private Payloads payloads;

	public Second_Stage() {
	}

	// created for testing purposes
	public Second_Stage(Payloads payloads) {
		this.payloads = payloads;
	}

	public Payloads getPayloads() {
		return payloads;
	}

	public void setPayloads(Payloads payloads) {
		this.payloads = payloads;
	}

	@Override
	public String toString() {
		return "Second_Stage [payloads=" + payloads + "]";
	}

}
