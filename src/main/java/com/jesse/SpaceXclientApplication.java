package com.jesse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


/**
 * @author Jesse Boyd
 *	note this needs to be at the package root in order to see sub folders
 *	This starts the spring boot application and uses it's auto configuration defaults
 *	Use Spring Tools Suite or add marketplace extension so you can 'run as Spring Boot Application'
 */
@SpringBootApplication(scanBasePackages = {"com.jesse"})
public class SpaceXclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpaceXclientApplication.class, args);
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	   // Do any additional configuration here
	   return builder.build();
	}
	
}
