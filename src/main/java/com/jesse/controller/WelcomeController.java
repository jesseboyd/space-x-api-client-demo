package com.jesse.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Jesse Boyd
 * A simple welcome screen with options to see rockets or launches pages
 */
@Controller
public class WelcomeController {

	@GetMapping(value = "/")
	public String index() {
		return "welcome";
	}

}
