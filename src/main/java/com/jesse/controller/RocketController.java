package com.jesse.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jesse.model.Rocket;
import com.jesse.service.RocketList;

/**
 * @author Jesse Boyd controller for different rocket display options presented
 *         on view rocket.jsp
 */
@Controller
public class RocketController {

	@Autowired
	private RocketList rList;

	private List<Rocket> rockets; 
	private List<Rocket> sortedRockets;

	@GetMapping("rockets")
	public String rockets(Model model) {
		if(rockets == null ) rockets = rList.getRocketList();
		model.addAttribute("rockets", rockets);
		return "rocket";
	}

	/**
	 * @param HttpServletRequest req autowired by Spring
	 * @return a list of rockets sorted alphabetically
	 */
	@GetMapping("sortRockets")
	public String sortRockets(Model model) {
		if (sortedRockets == null)
			sortedRockets = rockets.stream().sorted((a, b) -> a.getRocket_name().compareTo(b.getRocket_name()))
					.collect(Collectors.toList());
		model.addAttribute("rockets", sortedRockets);
		return "rocket";
	}

	/**
	 * @param user entered name of rocket autowired by Spring
	 * @return if name matches rocket will be returned else error message
	 */
	@PostMapping("rocketName")
	public String chooseByName(@RequestParam String name, Model model) {
		List<Rocket> sortedRockets = rockets.stream().filter(r -> r.getRocket_name().equals(name)).collect(Collectors.toList());
		model.addAttribute("rockets", sortedRockets);
		return "rocket";
	}

}
