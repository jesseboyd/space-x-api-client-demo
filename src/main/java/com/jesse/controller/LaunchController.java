package com.jesse.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.jesse.service.LaunchList;


/**
 * @author Jesse Boyd
 *	This controller will pull SpaceX launch data as JSON data and serve to launches.jsp view
 *	There could be more specific processing of the JSON data in the future
 *	Uses SpaceX_API_Client static methods to retrieve JSON payloads
 */
@Controller
public class LaunchController {
	
	@Autowired
	LaunchList launchList;

	@GetMapping(value = "launches")
	public String lauches(HttpServletRequest	 req) {
		req.setAttribute("dataHeader", "No launches requsted, choose a link above");
		return "launches";
	}
	
	@GetMapping(value = "allLaunches")
	public String allLaunches(HttpServletRequest	 req) {
		String allLaunches = launchList.getAllLaunches();
		req.setAttribute("dataHeader", "All Launches");
		req.setAttribute("launches", allLaunches );
		return "launches";
	}
	
	@GetMapping(value = "pastLaunches")
	public String pastLaunches(HttpServletRequest	 req) {
		String pastLaunches = launchList.pastLaunches();
		req.setAttribute("dataHeader", "Past Launches");
		req.setAttribute("launches", pastLaunches );
		return "launches";
	}
	
	@GetMapping(value = "futureLaunches")
	public String futureLaunches(HttpServletRequest	 req) {
		String futureLaunches = launchList.futureLaunches();
		req.setAttribute("dataHeader", "Future Launches");
		req.setAttribute("launches", futureLaunches );
		return "launches";
	}
	
}
