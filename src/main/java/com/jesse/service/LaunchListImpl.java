package com.jesse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class LaunchListImpl implements LaunchList {
	
	@Autowired
	RestTemplate restTemplate;

	@Override
	public String getAllLaunches() {
		return restTemplate.getForEntity("https://api.spacexdata.com/v3/launches", String.class).getBody();
	}

	@Override
	public String pastLaunches() {
		return restTemplate.getForEntity("https://api.spacexdata.com/v3/launches/past", String.class).getBody();
	}

	@Override
	public String futureLaunches() {
		return restTemplate.getForEntity("https://api.spacexdata.com/v3/launches/upcoming", String.class).getBody();
	}
	
}
