package com.jesse.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jesse.model.Rocket;

@Service
public class RocketListImpl implements RocketList  {

	@Autowired
	private RestTemplate restTemplate;
	
	private List<Rocket> rocketList;

	@Override
	public List<Rocket> getRocketList() {
		if (rocketList == null) {
			rocketList = (List<Rocket>) restTemplate.exchange("https://api.spacexdata.com/v3/rockets", HttpMethod.GET,
					null, new ParameterizedTypeReference<List<Rocket>>() {
					}).getBody();
		}
		return rocketList;
	}
	
	
}
