package com.jesse.service;

import java.util.List;

import com.jesse.model.Rocket;

public interface RocketList {

	List<Rocket> getRocketList();

}