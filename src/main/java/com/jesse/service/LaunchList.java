package com.jesse.service;

import org.springframework.stereotype.Component;

@Component
public interface LaunchList {

	String getAllLaunches();

	String pastLaunches();

	String futureLaunches();

}